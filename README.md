## Simple login app ##
* This simple login/register user management webapp.

### Setup ###
*  npm install
*  npm start


### for backend start ###
*  cd backend
*  npm install
*  npm start 

### For database ###
*  start mysql
*  create database
*  replace db credentials in .env file

*  DB_DIALECT=mysql
*  DB_HOST=localhost
*  DB_PORT=3306
*  DB_NAME=sample_db
*  DB_USER=root
*  DB_PASSWORD=


### update package.json if needed ###
#### Version Compatibility ####
| Node Version  | Bcrypt Version |
| ------------- | ------------- |
| 0.4  | <= 0.4  |
| 0.6, 0.8, 0.10  | <= 0.4  |
| 0.11  | >= 0.8  |
| 4  | < 2.1  |
| 8  | >= 1.0.3  |
| 10  | >= 3  |
