import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class HomePage extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    handleDeleteUser(id) {
        console.log(id);
        return (e) => this.props.dispatch(userActions.delete(id));
    }

    render() {

        const { user, users } = this.props;
        console.log(user.user);
        return (

            <div className={`content-section`}>
                <div className={`username`}>
                <span>Hi </span><strong>{user.user.first}! (<Link to="/login" className="btn-link">Logout</Link>)</strong>
                {users.loading && <em>Loading users...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                </div>
                {users.items &&
                   <div>
                    <table cellSpacing={`0`} cellPadding={`0`}>
                        <thead>
                        <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        {users.items.user.map((userinner, index) =>
                            <tr key={userinner.id}>
                                <td>{index+1}</td>
                                <td>{userinner.first + ' ' + userinner.last}</td>
                                <td>{userinner.email}</td>
                                <td>{userinner.phone}</td>
                                <td>
                                    {
                                        userinner.deleting ? <em> - Deleting...</em>
                                            : userinner.deleteError ?
                                            <span className="text-danger"> - ERROR: {userinner.deleteError}</span>
                                            : userinner.id ==  user.user.id ?
                                            <span className="text-danger">You</span>
                                            : <span> - <a onClick={this.handleDeleteUser(userinner.id)}>Delete</a></span>
                                    }
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                   </div>
                }
                <footer className="main-footer">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-sm-6">
                                <p>Powered by EZDEVS</p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };
